#! /usr/bin/python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-2.0
# Copyright (C) 2023 by Thorsten Leemhuis
__author__ = 'Thorsten Leemhuis <linux@leemhuis.info>'
__version__ = '0.0.1'

# NOTE: this script is still WIP and mainly tailored for my own needs.

# BEGIN ToDo list
#
# Here is a rough todo/ideas of things that could be done – or should be done
# if there is a community interest in the tool.
#
# * general
#  - reconsider the name for the script? 
#  - create man page and basic Makefile for installation
#  - support git parameters that might be helpful sometimes like '--since='
#  - some subcommands don't support stable properly yet
#  - check which subcommands need or might benefit from --next, --mainline, --stable
#  - related: is a "--branch" option needed for some subcommands or in general? Or just to better handle stable branches?
#  - the over-all story "accept single or multiple patterns" might be inconsistent
#  - configuration file for holding directory with the sources, names of the remotes (when needed), …
#  - support specifying the directory with the git tree
#  - fix FIXME entries
#  - [later] is gitpython actually needed/useful/worth it?
#  - [maybe] try to speed up some lookups by extracting some of the data to a sqlite cache; *might* be
#    useful for Gregs usecase, if faster than https://social.kernel.org/objects/7756d1a4-4ddd-4479-a16a-c95b0028efc5
#  - [maybe] make the script capable of creating, maintaining & updating the git repo
# * content
#  - support stable trees
# * define
#  - when used with a string accept slight differences when comparing commit summary with search pattern,
#    as the maintainers might have slightly adjust the patch summary while committing
# * maintainer
#   - implement "--no-git"
# * history
#  - support stable trees
#
# END ToDo list

# until this script supports a configuration file, you might have to add the name of your remote(s) to this dict
# to make it find the name of the remotes with Linux code
REPO_URLS_KNOWN = {'next': ('git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git',
                            'kernel.googlesource.com/pub/scm/linux/kernel/git/next/linux-next.git',
                            'gitlab.com/linux-kernel/linux-next.git'),
                   'mainline': ('git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
                                'kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git',
                                'github.com/torvalds/linux.git',
                                'gitlab.com/linux-kernel/linux.git'),
                   'stable': ('git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git',
                              'kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux.git'
                              'github.com/gregkh/linux.git',
                              'gitlab.com/linux-kernel/stable.git')}

# script starts here

import argparse
import configparser
import datetime
import pathlib
import re
import subprocess
import string
import sys
import urllib.parse

try:
    import git
except ModuleNotFoundError:
    print("Aborting, required module 'git' not found; please install 'GitPython'.")
    sys.exit(1)


class GitCommit():
    def __init__(self, gitobject, gittree, *, summary=None):
        self._gitobject = gitobject
        self._gittree = gittree
        self._summary = summary

        self._gitpyrepo = gittree.gitpyrepo
        self._authored_date = None
        self._authored_gmtime = None
        self._committed_date = None
        self._committed_gmtime = None
        self._hexsha_abbrev = None
        self._hexsha_full = None
        self._tag = None
        self._stable_commits = []

    @property
    def authored_date(self):
        if not self._authored_date:
            self._lookup_basics()
        return self._authored_date

    @property
    def authored_gmtime(self):
        if not self._authored_gmtime:
            self._lookup_basics()
        return self._authored_gmtime

    @property
    def committed_date(self):
        if not self._committed_date:
            self._lookup_basics()
        return self._committed_date

    @property
    def committed_gmtime(self):
        if not self._committed_gmtime:
            self._lookup_basics()
        return self._committed_gmtime

    @property
    def hexsha_abbrev(self):
        if not self._hexsha_abbrev:
            if self._gittree.is_hexsha(self._gitobject) and len(self._gitobject) >= self._gittree.abbrevlen:
                self._hexsha_abbrev = self._gitobject[0:self._gittree.abbrevlen]
            else:
                self._lookup_basics()
        return self._hexsha_abbrev

    @property
    def hexsha_full(self):
        if not self._hexsha_full:
            self._lookup_basics()
        return self._hexsha_full

    @property
    def summary(self):
        if not self._summary:
            self._lookup_basics()
        return self._summary

    @property
    def stable_commits(self):
        if not self._stable_commits:
            for branch in self._gittree.remotes['stable']:
                for commit in self._gittree.greplog(['Upstream commit %s' % self.hexsha_full,
                                                     'commit %s upstream' % self.hexsha_full],
                                                    branch, maxhits=1, since=self.authored_gmtime):
                    self._stable_commits.append(commit)
                    yield commit
        for commit in self._stable_commits:
            return commit

    @property
    def tag(self):
        if not self._tag:
            self._tag = self._get_tag()
        return self._tag

    def _lookup_basics(self):
        commit = self._gitpyrepo.commit(self._gitobject)
        self._authored_gmtime = commit.authored_date
        self._authored_date = datetime.datetime.fromtimestamp(self.authored_gmtime)
        self._committed_gmtime = commit.committed_date
        self._committed_date = datetime.datetime.fromtimestamp(self._committed_gmtime)
        self._hexsha_abbrev = commit.hexsha[0:self._gittree.abbrevlen]
        self._hexsha_full = commit.hexsha
        self._summary = commit.summary

    def _nextarrival(self):
        try:
            for tag in self._gitpyrepo.git.tag('--sort=creatordate', '--list', 'next-*').splitlines():
                earliest_known_tag = tag
                break
        except git.exc.GitCommandError as err:
            print("Aborting: error when trying to get tags for '%s':\n%s" %
                  (self._gitobject, err), file=sys.stderr)
            sys.exit(1)

        try:
            for tag in self._gitpyrepo.git.tag('--sort=creatordate', '--contains', self._gitobject, '--list', 'next-*').splitlines():
                break
        except git.exc.GitCommandError as err:
            print("Aborting: error when trying to get tags for '%s':\n%s" %
                  (self._gitobject, err), file=sys.stderr)
            sys.exit(1)

        if earliest_known_tag == tag:
            return 'prehistoric'
        return tag

    def _get_tag(self):
        remote = {}
        remote['remote'], remote['branch'] = self._gittree.branch_containing_commit(self._gitobject)
        # these matchfilters will speed things up a lot!
        if remote['remote'] == 'mainline':
            matchfilter = ["--exclude=next-*", "--exclude=v*.*.*"]
        elif remote['remote'] == 'next':
            matchfilter = ["--match=next-*"]
            # no need to query stable, commits have to be in mainline before they can be
            # backported (famous last words, I fear this might backfire sooner or later)
        elif remote['remote'] == 'stable':
            # remove the 'foo/linux-' prefix and the '.y' suffix
            prefixlen = remote['branch'].find('/linux-') + 7
            versionline = remote['branch'][prefixlen:-2]
            matchfilter = ["--match=v%s.*" % versionline]

        try:
            tag = self._gitpyrepo.git.describe('--tags', '--contains', '--abbrev=0', matchfilter, self._gitobject)
        except git.exc.GitCommandError as err:
            if remote['remote'] == 'mainline' and 'fatal: cannot describe' in (err.stderr):
                # commit was merged after the last tag, hence get the latest tag
                try:
                    for tag in self._gitpyrepo.git.tag('--sort=-creatordate', '--list').split('\n'):
                        if re.match(r"^v[0-9]*\.[0-9]*(-rc[0-9]*)?$", tag):
                            tag = tag + "-post"
                            break
                except git.exc.GitCommandError as err:
                    print("Aborting: error when trying to get latest tag for %s:\n%s" %
                          (remote['remote'], err), file=sys.stderr)
                    sys.exit(1)
            else:
                print("Aborting: error when trying to describe '%s' using --contains:\n%s" %
                      (self._gitobject, err), file=sys.stderr)
                sys.exit(1)

        for suffix in ('~', '^'):
            suffixstart = tag.find(suffix)
            if suffixstart > 0:
                tag = tag[:suffixstart]

        if tag.startswith('next-') and 'next-pending' in self._gittree.remotes:
            if self._gittree.is_in_branch(self._gitobject, self._gittree.remotes['next-pending'][0]):
                tag = tag + ' (pending-fixes)'

        return tag

    def print(self, *, printflags=[]):
        def _retrieve_tag_date(tag):
            tagcommit = GitCommit(tag, self._gittree)
            return tagcommit.committed_date

        print('%s ("%s") [' % (self.hexsha_abbrev, self.summary), flush=True, end='')

        if 'authordate' in printflags:
            print('authored: %s; ' % self.authored_date, flush=True, end='')

        if 'commitdate' in printflags:
            print('committed: %s; ' % self.committed_date, flush=True, end='')

        if 'nextarrival' in printflags:
            print('next arrival: %s; ' % self._nextarrival(), flush=True, end='')

        if 'mergedate' in printflags or 'topmerge' in printflags:
            topmerge = self._gittree.topmerge(self.hexsha_full)

            if 'mergedate' in printflags:
                print('merged: %s; ' % topmerge.committed_date, flush=True, end='')

            if 'topmerge' in printflags:
                print('merged via: "%s"; ' % topmerge.summary, flush=True, end='')

        print('%s' % self.tag, flush=True, end='')

        if 'releasedate' in printflags:
            if self.tag.endswith('-post') or self.tag.startswith('next-'):
                pass
            else:
                print(' (%s)' % _retrieve_tag_date(self.tag), flush=True, end='')

        if 'stable-tags' in printflags or 'stable-hexsha' in printflags:
            for stable_commit in self.stable_commits:
                print(', %s' % stable_commit.tag, flush=True, end='')
                optional_elements=[]
                if 'releasedate' in printflags:
                    optional_elements.append('%s' % _retrieve_tag_date(stable_commit.tag))
                if 'stable-hexsha' in printflags:
                    optional_elements.append(stable_commit.hexsha_abbrev)
                if optional_elements:
                    print(' (', end='')
                    print(', '.join(optional_elements), end='')
                    print(')', flush=True, end='')
        print(']')


class GitTree():
    def __init__(self):
        self._abbrevlen = None
        self._gitpyrepo = None
        self._remotes = {}

    @property
    def abbrevlen(self):
        if not self._abbrevlen:
            try:
                self._abbrevlen = self._gitpyrepo.config_reader().get_value("core", "abbrev")
            except configparser.NoOptionError:
                self._abbrevlen = 12
        return self._abbrevlen

    @property
    def gitpyrepo(self):
        if not self._gitpyrepo:
            self._gitpyrepo = git.Repo()
        return self._gitpyrepo

    @property
    def remotes(self):
        if not self._remotes:
            for remote in self.gitpyrepo.remotes:
                remote_url_full = urllib.parse.urlparse(remote.url)
                remote_url_stripped = '%s%s' % (remote_url_full.netloc, remote_url_full.path.rstrip("/"))
                mainline_in_next = None
                mainline_in_stable = None
                for tree in ('next', 'mainline', 'stable'):
                    if remote_url_stripped in REPO_URLS_KNOWN[tree]:
                        if tree == 'mainline':
                            self._remotes[tree] = ['%s/master' % remote.name]
                        elif tree == 'next':
                            self._remotes[tree] = ['%s/master' % remote.name]
                            for remote_branch in reversed(remote.refs):
                                if remote_branch.remote_head == 'stable':
                                    mainline_in_next = ['%s/stable' % remote.name]
                                elif remote_branch.remote_head == 'pending-fixes':
                                    self._remotes['next-pending'] = ['%s/pending-fixes' % remote.name]
                        elif tree == 'stable':
                            self._remotes[tree] = []
                            for remote_branch in reversed(remote.refs):
                                if remote_branch.remote_head in ('master', 'linux-rolling-stable', 'linux-rolling-lts'):
                                    if remote_branch.remote_head == 'master':
                                        mainline_in_stable = [remote_branch.name]
                                    continue
                                # ignore branches that haven't seen a commit in the past 3 weeks, as they are most
                                # likely EOL the following hack turned out to be quicker then using gitpython; maybe
                                # I did something stupid when I briefly tried using it…
                                result = subprocess.run(["git", "rev-list", "-n", "1",
                                                        "--since='3 week'", remote_branch.name],  capture_output=True)
                                if result.stdout:
                                    self._remotes['stable'].append(remote_branch.name)
                if 'mainline' not in self._remotes:
                    if mainline_in_stable:
                        self._remotes['mainline'] = mainline_in_stable
                    elif mainline_in_next:
                        self._remotes['mainline'] = mainline_in_next

        if 'mainline' not in self._remotes:
            print("\nAborting, could not find a remote containing Linux; either you are in the wrong directory, or you\
need to teach this script how to find a repo by adjusting the REPO_URLS_KNOWN constant.")
            sys.exit(1)

        return self._remotes

    @staticmethod
    def check_get_maintainer():
        for file in ('./scripts/get_maintainer.pl', 'MAINTAINERS'):
            if not pathlib.Path(file).is_file():
                return "%s not found. Need a checkout of Linux mainline or next to look up maintainers." % file

            diff_to_mainline = subprocess.run(["git", "diff", gittree.remotes['mainline'][0], '--', file], capture_output=True).stdout
            diff_to_next = None
            if 'next' in gittree.remotes:
                diff_to_next = subprocess.run(["git", "diff", gittree.remotes['next'][0], '--', file], capture_output=True).stdout
            if not diff_to_mainline or not diff_to_next:
                pass
            else:
                return "the file %s is not up to date in checkout." % file


    def determine_master_or_next(self, pattern):
        if 'next' not in self.remotes:
            return self.remotes['mainline'][0]

        # check if mainline and next diverged
        fork_point = self.gitpyrepo.git.merge_base(self.remotes['mainline'][0], self.remotes['next'][0])
        changed_in_mainline = self.gitpyrepo.git.rev_list(
            '-1', '%s..%s' % (fork_point, self.remotes['mainline'][0]), '--', pattern)
        changed_in_next = self.gitpyrepo.git.rev_list(
            '-1', '%s..%s' % (fork_point, self.remotes['next'][0]), '--', pattern)
        diff_mainline_next = self.gitpyrepo.git.diff('%s..%s' % (
            self.remotes['mainline'][0], self.remotes['next'][0]), '--', pattern)

        if not diff_mainline_next:
            return self.remotes['mainline'][0]
        elif changed_in_mainline and changed_in_next:
            # RECONSIDER: maybe also show the last ~10 commits for each branch using a format like git log --oneline?
            print("Aborting, file changed in both mainline and next; specify tree to check using '--next' or '--mainline'")
            sys.exit(1)
        elif changed_in_mainline:
            return self.remotes['mainline'][0]
        return self.remotes['next'][0]

    def is_gitobject(self, pattern):
        try:
            self.gitpyrepo.git.rev_parse('-q', '--verify', "%s^{object}" % pattern)
        except git.exc.GitCommandError:
            return False
        return True

    @staticmethod
    def is_hexsha(pattern):
        if len(pattern) < 6 or len(pattern) > 40:
            return False
        return set(pattern).issubset(string.hexdigits)

    def is_file(self, pattern):
        if self.gitpyrepo.git.rev_list('-1', self.remotes['mainline'][0], '--', pattern):
            return True
        return False

    def is_in_branch(self, gitobject, branch):
        try:
            self._gitpyrepo.git.merge_base('--is-ancestor', gitobject, branch)
            return True
        except git.exc.GitCommandError:
            pass
        return False


    def branch_containing_commit(self, gitobject):
        for remote in ('mainline', 'next', 'stable'):
            if remote not in self.remotes:
                continue
            for remotebranch in self.remotes[remote]:
                try:
                    self.gitpyrepo.git.merge_base('--is-ancestor', gitobject, remotebranch)
                    return remote, remotebranch
                except git.exc.GitCommandError:
                    pass
        # reminder: this might happen if the commit is in the remote, but not in a branch – for example
        # a commits that was dropped from -next
        print("Aborting: object '%s' not found in any of the active remotes branches." % gitobject, file=sys.stderr)
        sys.exit(1)

    def greplog(self, patterns, branch, *, maxhits=None, since=None, ignorecase=False):
        if isinstance(patterns, str):
            patterns = (patterns, )

        # using subprocess here, as gitpython doesn't allow to parse the results on-the fly
        cmdline = ['/usr/bin/git', 'log', '--oneline',  '--pretty=%H %s']
        if ignorecase:
            cmdline.append('--regexp-ignore-case')
        for pattern in patterns:
            cmdline.append('--grep=%s' % pattern)
        if since:
            cmdline.append('--since=%s' % since)
        cmdline.append(branch)
        with subprocess.Popen(cmdline, stdout=subprocess.PIPE, bufsize=1, universal_newlines=True) as p:
            for count, line in enumerate(p.stdout):
                yield GitCommit(line[0:39], self, summary=line[41:-1])

                if maxhits and count == maxhits - 1:
                    p.terminate()
                    return

        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, p.args)

    def grep(self, pattern, branch=None, stable=False, subject_only=False):
        if branch:
            branches = (branch, )
        elif 'next' not in self.remotes:
            branches = (self.remotes['mainline'][0], )
        else:
            branches = ('%s..%s' % (self.remotes['mainline'][0], self.remotes['next'][0]), self.remotes['mainline'][0])

        for branch in branches:
            for commit in self.greplog(pattern, branch, ignorecase=True):
                if subject_only and pattern.casefold() not in commit.summary.casefold():
                    continue
                yield commit

    def topmerge(self, gitobj):
        commit = GitCommit(gitobj, self)
        _, remotebranch = self.branch_containing_commit(gitobj)

        # check if its a top-level commit, as we just need to print it then
        # Side note: guess sooner or later doing it this way will become to slow…
        toplevel_commits = self.gitpyrepo.git.rev_list('--first-parent', remotebranch).splitlines()
        if commit.hexsha_full in toplevel_commits:
            return commit
        toplevel_commits = None

        # inspired by https://stackoverflow.com/a/20615706: find the last result
        # in ancestry_path that's also in first_parent
        ancestry_path = self.gitpyrepo.git.rev_list(
            '--ancestry-path', "%s..%s" % (gitobj, remotebranch)).splitlines()
        first_parent = self.gitpyrepo.git.rev_list(
            '--first-parent', "%s..%s" % (gitobj, remotebranch)).splitlines()
        for hexsha in reversed(ancestry_path):
            if hexsha in first_parent:
                return GitCommit(hexsha, self)
                break


def cmd_content(gittree, cmdargs):
    if not gittree.is_file(cmdargs.pattern):
        print("Aborting, file '%s' not found" % cmdargs.pattern)
        sys.exit(1)

    # FIXME: support for stable
    if cmdargs.next:
        if 'next' not in gittree.remotes:
            print("Aborting, '--next' provided, but did not find a remote for linux-next\
 (maybe you need to adjust this script's REPO_URLS_KNOWN variable).")
            sys.exit(1)
        branch = gittree.remotes['next'][0]
    elif cmdargs.mainline:
        branch = gittree.remotes['mainline'][0]
    else:
        branch = gittree.determine_master_or_next(cmdargs.pattern)
    subprocess.run(["git", "show", '%s:%s' % (branch, cmdargs.pattern)])


def cmd_define(gittree, cmdargs):
    printflags=[]
    if cmdargs.stable_hexsha:
        printflags.append('stable-hexsha')
    elif cmdargs.stable:
        printflags.append('stable-tags')
    if cmdargs.dates or cmdargs.authordate:
        printflags.append('authordate')
    if cmdargs.dates or cmdargs.commitdate:
        printflags.append('commitdate')
    if cmdargs.dates or cmdargs.mergedate:
        printflags.append('mergedate')
    if (cmdargs.dates and 'next' in gittree.remotes) or cmdargs.nextarrival:
        printflags.append('nextarrival')
    if cmdargs.dates or cmdargs.releasedate:
        printflags.append('releasedate')
    if cmdargs.topmerge:
        printflags.append('topmerge')

    if (cmdargs.stable or cmdargs.stable_hexsha) and 'stable' not in gittree.remotes:
        print("Aborting, '--stable' provided, but did not find a remote for linux-stable\
 (maybe you need to adjust this script's REPO_URLS_KNOWN variable).")
        sys.exit(1)

    if cmdargs.nextarrival and 'next' not in gittree.remotes:
        print("Aborting, '--nextarrival' provided, but did not find a remote for linux-next\
 (maybe you need to adjust this script's REPO_URLS_KNOWN variable).")
        sys.exit(1)

    for pattern in cmdargs.patterns:
        if gittree.is_gitobject(pattern):
            GitCommit(pattern, gittree).print(printflags=printflags)
            continue
        if gittree.is_hexsha(pattern):
            print("Not object '%s' known" % pattern, file=sys.stderr)
            continue

        count = -1
        for commit in gittree.grep(pattern, subject_only=True):
            commit.print(printflags=printflags)
            count += 1
        if count >= 0:
            if len(cmdargs.patterns) > 1:
                print("[This concludes the list of results for the pattern '%s']" % pattern)
            continue
        print("Found nothing matching '%s'" % pattern, file=sys.stderr)


def cmd_greplog(gittree, cmdargs):
    for commit in gittree.grep(cmdargs.pattern):
        commit.print()


def cmd_history(gittree, cmdargs):
    # FIXME: support for stable
    if not gittree.is_file(cmdargs.patterns):
        print('Skipping "%s", file not found in %s' % (cmdargs.patterns, gittree.remotes['mainline'][0]))

    if cmdargs.next:
        branch = gittree.remotes['next'][0]
    elif cmdargs.mainline:
        branch = gittree.remotes['mainline'][0]
    else:
        branch = gittree.determine_master_or_next(cmdargs.patterns)
    subprocess.run(["git", "log", '--no-merges', branch, '--', cmdargs.patterns])


def cmd_maintainer(gittree, cmdargs):
    # wondering if there is some way to run get_maintainers.pl without a checkout; if you do know one, tell me;
    # until then let's to it like this:
    result = gittree.check_get_maintainer()
    if result:
        print("Aborting: %s" % result)
        sys.exit(1)

    subprocess.run(['scripts/get_maintainer.pl', cmdargs.pattern])


def cmd_show(gittree, cmdargs):
    pattern = cmdargs.pattern
    if gittree.is_gitobject(pattern):
        subprocess.run(["git", "show", '--pretty=fuller', pattern])
        return

    results = []

    def _flushresults(results):
        print("\nThat was just the first commit that matched '%s'; there are more:" % pattern)
        for commit in results:
            commit.print()

    p = None
    for count, commit in enumerate(gittree.grep(pattern, subject_only=True)):
        # show first commit found
        if count == 0:
            p = subprocess.Popen(['git', 'show', '--pretty=fuller', commit.hexsha_abbrev])

        # search for further hits while git show is running
        if p.poll() is None:
            results.append(commit)
            _ = commit.tag
            continue

        # process finished, so flush the results we gathered so far
        if len(results) > 0:
            results.append(commit)
            _flushresults(results)
            results = []
            continue

        # new result after process finished and results were flushed
        commit.print()

    if p:
        p.wait()
        print
        # flush results in case we haven't yet
        if count > 0 and len(results) > 0:
            _flushresults(results)
        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, p.args)


def cmd_selfcheck(gittree, cmdargs):
    print('== remotes ==')
    for tree in ('next', 'mainline', 'stable'):
        if tree in gittree.remotes:
            result = gittree.remotes[tree]
        else:
            result = 'not found'
        print('%s: %s' % (tree, result))

    print('== get_maintainer.pl ==')
    result = gittree.check_get_maintainer()
    if not result:
        result = 'get_maintainer.pl and MAINTAINERS found and up to date'
    print('get_maintainer.pl: %s' %result )

def cmd_topmerge(gittree, cmdargs):
    pattern = cmdargs.pattern
    if gittree.is_gitobject(pattern):
        gitcommit = gittree.topmerge(pattern)
        gitcommit.print()
    else:
        for result in gittree.grep(pattern, subject_only=True):
            gitcommit = gittree.topmerge(result.hexsha_full)
            gitcommit.print()


def parse_commandline():
    # basics
    parser = argparse.ArgumentParser(
        prog='linuxscru',
        description='Scrutiny tool for a Linux git clone',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument('--version', action='version',
                        version=__version__)

    parser.add_argument('--selfcheck', action='store_true',)

    # subcommands
    subparsers = parser.add_subparsers(help='sub-command help', dest='subcmd')

    # - content
    sparser_content = subparsers.add_parser('content',
                                            help="show a file's content")
    sparser_content.add_argument('--next', action='store_true', help='query only linux-next')
    sparser_content.add_argument('--mainline', action='store_true', help='query only linux-next')
    sparser_content.add_argument('pattern', help='file to show log')
    sparser_content.set_defaults(func=cmd_content)

    # - define
    sparser_define = subparsers.add_parser('define',
                                           help='briefly list matching commits')
    sparser_define.add_argument('--authordate', action='store_true', help='show when commit was authored')
    sparser_define.add_argument('--commitdate', action='store_true', help='show when commit was committed')
    sparser_define.add_argument('--dates', action='store_true', help='show dates for all major events')
    sparser_define.add_argument('--mergedate', action='store_true', help='show when commit was merged')
    sparser_define.add_argument('--nextarrival', action='store_true', help='show oldest next tag containing the commit')
    sparser_define.add_argument('--releasedate', action='store_true', help='show when a version with commit was tagged')
    sparser_define.add_argument('--stable', action='store_true',help='show tags for stable trees')
    sparser_define.add_argument('--stable-hexsha', action='store_true',
                                help='show tags and hexsha for stable trees, too')
    sparser_define.add_argument('--topmerge', action='store_true',
                                help='show tree through with the commit was merged')
    sparser_define.add_argument('patterns', help='gitobjects or search patterns to define', nargs='+')
    sparser_define.set_defaults(func=cmd_define)

    # - greplog
    sparser_greplog = subparsers.add_parser('greplog', help='list commits containing a specific pattern')
    sparser_greplog.add_argument('pattern', help='pattern to search')
    sparser_greplog.set_defaults(func=cmd_greplog)

    # - history
    sparser_history = subparsers.add_parser('history',
                                            help='list history of a file')
    sparser_history.add_argument('--next', action='store_true', help='query only linux-next')
    sparser_history.add_argument('--mainline', action='store_true', help='query only linux-next')
    sparser_history.add_argument('patterns', help='file(s) to show log')
    sparser_history.set_defaults(func=cmd_history)

    # - maintainer
    sparser_maintainer = subparsers.add_parser('maintainer',
                                               help='run get_maintainer.pl')
    sparser_maintainer.add_argument('pattern', help='file to check')
    sparser_maintainer.set_defaults(func=cmd_maintainer)

    # show
    sparser_show = subparsers.add_parser('show', help='show a specific commit')
    sparser_show.add_argument('pattern', help='object to show')
    sparser_show.set_defaults(func=cmd_show)

    # topmerge
    sparser_topmerge = subparsers.add_parser('topmerge', help='show top-level commit containing specified commit')
    sparser_topmerge.add_argument('pattern', help='gitobject (commit, tag, ...) or search pattern to check')
    sparser_topmerge.set_defaults(func=cmd_topmerge)

    # go
    cmdargs = parser.parse_args()
    # make --check act like a proper subcommand internally
    if cmdargs.selfcheck:
        cmdargs.func = cmd_selfcheck
    if 'func' not in cmdargs:
        parser.print_help()
        sys.exit(1)

    return cmdargs


def seltcheck(gittree):
    if newline:
        print()
    sys.exit(returncode)

def exit_gracefully(returncode=1, newline=True):
    if newline:
        print()
    sys.exit(returncode)


if __name__ == '__main__':
    try:
        cmdargs = parse_commandline()
        gittree = GitTree()
        cmdargs.func(gittree, cmdargs)
    except KeyboardInterrupt:
        # this will happen often, so exit gracefully
        exit_gracefully(130)
    except BrokenPipeError:
        sys.stderr.close()
        exit_gracefully(1)
