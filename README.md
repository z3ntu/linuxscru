linuxscru: a Linux kernel git repo scrutiny tool
================================================

Linuxscru is a *WIP* script of alpha quality that facilitates some queries to
git repos containing the Linux kernel – either mainline, next, and stable or all
at once. Right now the script is not really made for a wide audience, as some
things are tailored to the needs of linuxscru's author – which is also why some
obvious features and better error handling are missing. That could change if a
wider interest in linuxscru should emerge , especially if people send patches.

Here are a few examples of linuxscru's capabilities:

```
[thl@truhe all]$ linuxscru greplog "foobar" | head -n 3
0c612f9e77cc2c ("btrfs: stop doing excessive space reservation for csum deletion") [next-20230926]
e5e1e6d28ebcc0 ("drm/i915/pxp: Add MTL helpers to submit Heci-Cmd-Packet to GSC") [v6.5-rc1]
d69b5a90e17dca ("Merge branch 'selftests/bpf: support custom per-test flags and multiple expected messages'") [v6.4-rc1]

[thl@truhe all]$ linuxscru topmerge 13619170303878e
1c9f8dff62d85c ("Merge tag 'char-misc-6.6-rc1' of git://git.kernel.org/pub/scm/linux/kernel/git/gregkh/char-misc") [v6.6-rc1]

[thl@truhe all]$ linuxscru define 13619170303878e
13619170303878 ("interconnect: Teach lockdep about icc_bw_lock order") [v6.6-rc1]

[thl@truhe all]$ linuxscru define 13619170303878e --stable
13619170303878 ("interconnect: Teach lockdep about icc_bw_lock order") [v6.6-rc1, v6.5.5, v6.1.55, v5.15.133]

[thl@truhe all]$ linuxscru define 6c73daf26420b97
6c73daf26420b9 ("rtla/timerlat_aa: Fix negative IRQ delay") [v6.6-rc4-post]

[thl@truhe all]$ linuxscru define 7d730f1bf6f39ec
7d730f1bf6f39e ("Add linux-next specific files for 20231005") [next-20231005]

[thl@truhe all]$ linuxscru define "interconnect: Teach lockdep about icc_bw_lock order"
13619170303878 ("interconnect: Teach lockdep about icc_bw_lock order") [v6.6-rc1]

[thl@truhe all]$ linuxscru define "interconnect:" | head -n 3
770c69f037c18c ("interconnect: Add debugfs test client") [v6.6-rc1]
1d13d3b745377f ("interconnect: Reintroduce icc_get()") [v6.6-rc1]
a18e26a58bf3d5 ("interconnect: qcom: icc-rpmh: Retire DEFINE_QBCM") [v6.6-rc1]

```

Besides those four subcommands there are four more:

  * `linuxscru content`: shows the content of a file using 'git show'.
  * `linuxscru history`: shows the log of a file using 'git log'.
  * `linuxscru maintainer`: run ./scripts/get_maintainer.pl on a file (needs a
     up to date checkout of either mainline or next).
  * `linuxscru show`: shows a commit using  'git show'; if used with a search
     string shows the first hit and afterwards lists others.

The subcommands 'content' and 'history' query linux-next by default if
available, otherwise mainline. Both warn if the file was changed in mainline
after -next branched off; use the flags '--mainline' or '--next' to specify the
branch to query.

Nothing linuxscru does is rocket science: all the queries can be achieved with
a single command or a combination of multiple commands. It's just that the
original author of linuxscru (Thorsten Leemhuis) had to perform these queries
extremely often for his work as Linux kernel regression tracker. He thus
started linuxscru to make these queries a little bit easier to perform to
increase the quality of life – and hopefully at some point safe time, too
(https://xkcd.com/1319/). 

Run `linuxscru --selfcheck` to perform a check if everything required is found.
In case the script does not find the remotes with the Linux sources
automatically, you might need to adjust the REPO_URLS_KNOWN variable (until
support for a config file is implemented).

Note that linuxscru works with what's it finds in the git repo. If it only
contains mainline, it thus won't be able to search next or mention to which
stable trees a patch was backported to. The situation is better if you use a
git clone that contain next or stable, as both also contain mainline. But keep
in mind: the mainline codebase in the stable repo slightly lags behind upstream
and the mainline codebase in next is only updated on release of a new next
snapshot.
