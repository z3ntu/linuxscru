# poor man's CI

set -e

run_tests()
{
	echo $'\n''### content 1'
	~/usr/git/kernel-scripts/linuxscru/linuxscru content Documentation/.gitignore
	echo $'\n''### content 2'
	~/usr/git/kernel-scripts/linuxscru/linuxscru content Documentation/.gitignore --next
	echo $'\n''### content 3'
	~/usr/git/kernel-scripts/linuxscru/linuxscru content Documentation/.gitignore --mainline
	echo $'\n''### define 1'
	~/usr/git/kernel-scripts/linuxscru/linuxscru define v6.5
	echo $'\n''### define 2'
	~/usr/git/kernel-scripts/linuxscru/linuxscru define 13619170303878e
	echo $'\n''### define 3'
	~/usr/git/kernel-scripts/linuxscru/linuxscru define 13619170303878e --stable
	echo $'\n''### greplog'
	~/usr/git/kernel-scripts/linuxscru/linuxscru greplog "Teach lockdep that icc_bw_lock is needed in code paths that could"
	echo $'\n''### history'
	~/usr/git/kernel-scripts/linuxscru/linuxscru history Documentation/.gitignore
	echo $'\n''### maintainer'
	~/usr/git/kernel-scripts/linuxscru/linuxscru maintainer Documentation/.gitignore
	echo $'\n''### show 1'
	~/usr/git/kernel-scripts/linuxscru/linuxscru show v6.5
	echo $'\n''### show 2'
	~/usr/git/kernel-scripts/linuxscru/linuxscru show 13619170303878e
	echo $'\n''### show 3'
	~/usr/git/kernel-scripts/linuxscru/linuxscru show "interconnect: Teach lockdep about icc_bw_lock order"
	echo $'\n''### topmerge 1'
	~/usr/git/kernel-scripts/linuxscru/linuxscru topmerge 13619170303878e
	echo $'\n''### topmerge 2'
	~/usr/git/kernel-scripts/linuxscru/linuxscru topmerge "interconnect: Teach lockdep about icc_bw_lock order"
	echo $'\n''### topmerge 3'
	~/usr/git/kernel-scripts/linuxscru/linuxscru topmerge bbb8ceb5e24211
}

run_tests 2>&1 | tee ~/usr/git/linuxscru/.result.new || :
echo '#################################################################'$'\n'
if diff -Naur ~/usr/git/linuxscru/.result ~/usr/git/linuxscru/.result.new; then
	echo $'\n''SUCCEEDED'
else
	echo $'\n''FAILED'
	exit 1
fi
rm ~/usr/git/kernel-scripts/linuxscru/.result.new

